'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
//main page controller
  .controller('graphCtrl', ['$scope', function($scope) {
  	 $scope.myValues = [[0,2,3,4],[9,6,4,3]];
        $scope.myObj = {
          series:[
                {
                    lineColor:"#900000",
                    marker:{
                        backgroundColor:"#dc3737",
                        borderWidth:1,
                        shadow:0,
                        borderColor:"#f56b6b"
                    }
                },
                {
                    lineColor:"#efe634",
                    marker:{
                        backgroundColor:"#fff41f",
                        borderWidth:1,
                        shadow:0,
                        borderColor:"#fdffc0"
                    }
                },
            ]
        };
        var myConfig = {
        "graphset":[
            {
                "type":"bar",
                "plot":{
                    "stacked":0,
                    "line-width":2,
                    "value-box":{
                        "visible":0
                    },
                    "animation":{
                      "effect":"ANIMATION_FADE_IN",
                      "speed":"2000"
                    },
                },
                // "shapes":[
                //     {
                //         "type":"rect",
                //         "id":"animation",
                //         "width":"100",
                //         "height":"30",
                //         "background-color":"#666 #333",
                //         "border-radius":"10px",
                //         "x":"50px",
                //         "y":"15px",
                //         "label":{
                //             "text":"Start Animation",
                //             "font-color":"#fff"
                //         }
                //     }
                // ],
                "series":[
                    {
                        "values":[34,102,131,80,134,151,99,27,29,126,112,77,112,70,47,138,163,34,116,38,154,124,39,33,76,118,146,108,80,99]
                    },
                    {
                        "values":[186,118,189,65,109,129,74,195,65,141,76,194,134,155,99,80,167,201,92,172,93,145,148,153,166,147,74,108,159,83]
                    },
                    {
                        "values":[245,137,126,233,168,141,186,221,153,155,135,153,130,233,154,189,216,147,100,186,204,139,204,140,203,151,152,120,183,135]
                    },
                    {
                        "values":[191,251,226,168,239,283,148,155,269,165,150,232,221,207,227,255,144,207,204,182,261,240,193,179,244,289,204,205,222,288]
                    }
                ]
            }
        ]
    };
     
    zingchart.render({ 
        id : 'myChart', 
        data : myConfig, 
        height : "100%", 
        width: "100%"
    });
     
    zingchart.shape_click = function(p) {
      if (p.shapeid == 'animation') {
        zingchart.exec('myChart', 'reload');
      }
    }

        var myConfig = {
          "type":"pie",
          // "title":{
          //   "text":"Pie Chart"
          // },
          "series":[
            {"values":[59]},
            {"values":[55]},
            {"values":[30]},
            {"values":[28]},
            {"values":[15]}
          ]
        };

        zingchart.render({ 
            id : 'myPieChart', 
            data : myConfig, 
            height: 250, 
            width: "100%" 
        });

}]);